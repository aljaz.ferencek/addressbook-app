from django.shortcuts import render
from .models import Employee
from django.http import HttpResponse
from django.views import View
import statistics

class IndexView(View):
    def get(self, request):
        employees = Employee.objects.all()
        context = {
            'employees': employees
        }
        return render(request, 'index.html', context)

class AnalyticsView(View):
    def get(self, request):
        employees = Employee.objects.all()
        
        employee_ages = [employee.age for employee in employees]

        # Average Age
        average_age = sum(employee_ages) / len(employee_ages)

        # Median Age
        median_age = statistics.median(employee_ages)
        
        # Max Salary
        employee_salary = [employee.salary for employee in employees]

        max_salary = max(employee_salary)

        # Employees Ratio
        females_count = Employee.objects.filter(gender='F').count()
        males_count = Employee.objects.filter(gender='M').count()
        all_employees = females_count + males_count
        
        male_ratio = males_count / all_employees
        female_ratio = females_count / all_employees

        
        context = {
            'average_age': average_age,
            'median_age': median_age,
            'max_salary': max_salary,
            'male_ratio': male_ratio,
            'female_ratio': female_ratio
        }

        return render(request, 'analitika.html', context)

