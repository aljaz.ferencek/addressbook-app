from django.db import models
from django.db.models import Avg, Max, Min, Sum
from datetime import datetime

class Employee(models.Model):
    class Meta:
        db_table = 'employees'
        verbose_name = 'Employee'
        verbose_name_plural = 'Employees'
        ordering = ['id']

    name = models.CharField(verbose_name='Name', max_length=100, default='John Doe')
    birthday_date = models.DateField(verbose_name='Birthday date')

    GENDER_CHOICES = (
        ('M', 'Male'),
        ('F', 'Female'),
    )

    gender = models.CharField(max_length=1, choices=GENDER_CHOICES, verbose_name='Gender')
    salary = models.DecimalField(verbose_name='Salary', decimal_places=2, max_digits=10)

    def __str__(self):
        return self.name

    @property
    def age(self):
        return int((datetime.now().date() - self.birthday_date).days / 365.25)
    

