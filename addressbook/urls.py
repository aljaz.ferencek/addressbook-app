from django.urls import path
from django.conf.urls import url

from . import views

urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('analytics/', views.AnalyticsView.as_view(), name = 'analytics'),
]