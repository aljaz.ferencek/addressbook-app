# Generated by Django 3.0.3 on 2020-02-16 18:14

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('addressbook', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='employee',
            name='name',
            field=models.CharField(default='John Doe', max_length=100, verbose_name='Name'),
        ),
    ]
