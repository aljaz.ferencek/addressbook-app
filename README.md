Installation and application set up
1. Check Python version $python -m django --version
2. If Python is not installed do install it 
3. Import Django in Python shell with >>> import django
4. Install PIP package to Python
5. Import statistics $pip install statistics
6. Move to project directory and run $ python manage.py runserver
7. Open URL: http://127.0.0.1:8000/


Application use
1. Vist http://127.0.0.1:8000/admin/ to view and add employees
2. Visit http://127.0.0.1:8000/address-book/ to view all employees
3. Visit http://127.0.0.1:8000/address-book/analytics/ to view Analytics

Stop server
1. Stop the server in CMD with ctrl+c

Issues
- Due to the lack of time and knowledge I didn't manage to make Public Profile page